" vim:set ts=8 sts=2 sw=2 tw=0:
"
" matrix.vim - MATRIX like colorscheme.
"
" Maintainer:	MURAOKA Taro <koron@tka.att.ne.jp>
" Last Change:  10-Jun-2003.

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = 'matrix'

" the character under the cursor
hi Cursor	guifg=#226622 ctermfg=22 guibg=#55ff55 ctermbg=83
hi lCursor	guifg=#226622 ctermfg=22 guibg=#55ff55 ctermbg=83
" like Cursor, but used when in IME mode |CursorIM|
hi CursorIM	guifg=#226622 ctermfg=22 guibg=#55ff55 ctermbg=83
" directory names (and other special names in listings)
hi Directory	guifg=#55ff55 ctermfg=83 guibg=#000000 ctermbg=0
" diff mode: Added line |diff.txt|
hi DiffAdd	guifg=#55ff55 ctermfg=83 guibg=#226622 ctermbg=22 gui=none
" diff mode: Changed line |diff.txt|
hi DiffChange	guifg=#55ff55 ctermfg=83 guibg=#226622 ctermbg=22 gui=none
" diff mode: Deleted line |diff.txt|
hi DiffDelete	guifg=#113311 ctermfg=234 guibg=#113311 ctermbg=234 gui=none
" diff mode: Changed text within a changed line |diff.txt|
hi DiffText	guifg=#55ff55 ctermfg=83 guibg=#339933 ctermbg=65 gui=bold
" error messages on the command line
hi ErrorMsg	guifg=#55ff55 ctermfg=83 guibg=#339933 ctermbg=65
" the column separating vertically split windows
hi VertSplit	guifg=#339933 ctermfg=65 guibg=#339933 ctermbg=65
" line used for closed folds
hi Folded	guifg=#44cc44 ctermfg=77 guibg=#113311 ctermbg=234
" 'foldcolumn'
hi FoldColumn	guifg=#44cc44 ctermfg=77 guibg=#226622 ctermbg=22
" 'incsearch' highlighting; also used for the text replaced with
hi IncSearch	guifg=#226622 ctermfg=22 guibg=#55ff55 ctermbg=83 gui=none
" line number for ":number" and ":#" commands, and when 'number'
hi LineNr	guifg=#44cc44 ctermfg=77 guibg=#000000 ctermbg=0
" 'showmode' message (e.g., "-- INSERT --")
hi ModeMsg	guifg=#44cc44 ctermfg=77 guibg=#000000 ctermbg=0
" |more-prompt|
hi MoreMsg	guifg=#44cc44 ctermfg=77 guibg=#000000 ctermbg=0
" '~' and '@' at the end of the window, characters from
hi NonText	guifg=#44cc44 ctermfg=77 guibg=#113311 ctermbg=234
" normal text
hi Normal	guifg=#44cc44 ctermfg=77 guibg=#000000 ctermbg=0
" |hit-enter| prompt and yes/no questions
hi Question	guifg=#44cc44 ctermfg=77 guibg=#000000 ctermbg=0
" Last search pattern highlighting (see 'hlsearch').
hi Search	guifg=#113311 ctermfg=234 guibg=#44cc44 ctermbg=77 gui=none
" Meta and special keys listed with ":map", also for text used
hi SpecialKey	guifg=#44cc44 ctermfg=77 guibg=#000000 ctermbg=0
" status line of current window
hi StatusLine	guifg=#55ff55 ctermfg=83 guibg=#339933 ctermbg=65 gui=none
" status lines of not-current windows
hi StatusLineNC	guifg=#113311 ctermfg=234 guibg=#339933 ctermbg=65 gui=none
" titles for output from ":set all", ":autocmd" etc.
hi Title	guifg=#55ff55 ctermfg=83 guibg=#113311 ctermbg=234 gui=bold
" Visual mode selection
hi Visual	guifg=#55ff55 ctermfg=83 guibg=#339933 ctermbg=65 gui=none
" Visual mode selection when vim is "Not Owning the Selection".
hi VisualNOS	guifg=#44cc44 ctermfg=77 guibg=#000000 ctermbg=0
" warning messages
hi WarningMsg	guifg=#55ff55 ctermfg=83 guibg=#000000 ctermbg=0
" current match in 'wildmenu' completion
hi WildMenu	guifg=#226622 ctermfg=22 guibg=#55ff55 ctermbg=83

hi Comment	guifg=#226622 ctermfg=22 guibg=#000000 ctermbg=0
hi Constant	guifg=#55ff55 ctermfg=83 guibg=#226622 ctermbg=22
hi Special	guifg=#44cc44 ctermfg=77 guibg=#226622 ctermbg=22
hi Identifier	guifg=#55ff55 ctermfg=83 guibg=#000000 ctermbg=0
hi Statement	guifg=#55ff55 ctermfg=83 guibg=#000000 ctermbg=0 gui=bold
hi PreProc	guifg=#339933 ctermfg=65 guibg=#000000 ctermbg=0
hi Type		guifg=#55ff55 ctermfg=83 guibg=#000000 ctermbg=0 gui=bold
hi Underlined	guifg=#55ff55 ctermfg=83 guibg=#000000 ctermbg=0 gui=underline
hi Error	guifg=#55ff55 ctermfg=83 guibg=#339933 ctermbg=65
hi Todo		guifg=#113311 ctermfg=234 guibg=#44cc44 ctermbg=77 gui=none
