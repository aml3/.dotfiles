#! /bin/bash

DEBUG=true

SCRIPT_NAME=$(echo "$0" | sed 's/\.\///') # Remove the leading "./".
BACKUP_DIR="backups"
RED='\033[1;31m'
RESET='\033[0m'

debugEcho () { if $DEBUG; then
    echo "$@"
  fi
}

debugPrintf() {
  if $DEBUG; then
    printf "$@"
  fi
}

moveAndBackup () {
  local src="$1" # relative

  if [ -d "$src" ]; then
    find "$src" -type f -exec echo "{}" \; | while read f
    do 
      debugEcho -e "on srcFile=$f"
      moveAndBackup "$f"
    done
  else
    local backupLoc="$(pwd)/$BACKUP_DIR/$src"
    local absoluteDest="$HOME/$src" # absolute
    if [ -e "$absoluteDest" ]; then
      mkdir -p "$(dirname $backupLoc)"
      cp "$absoluteDest" "$backupLoc"
    fi

    local absoluteSrc="$(pwd)/$src"
    echo "ln -f -s $absoluteSrc $absoluteDest"
  fi
}

find . -maxdepth 1 -exec echo "{}" \; | while read f
do
  f="$(echo ${f#./})"
  debugPrintf "on %s" "$f"
  if [[ "$f" =~ ^$SCRIPT_NAME$ ]] \
    || [[ "$f" =~ ^$BACKUP_DIR$ ]] \
    || [[ "$f" =~ ^.$ ]] \
    || [[ "$f" =~ ^.*swp$ ]] \
    || [[ "$f" =~ ^.gitignore$ ]] \
    || [[ "$f" =~ ^.git$ ]]; then
    printf "${RED} skipping %s${RESET}\n" "$f"
    continue
  else
    debugPrintf "\n"
  fi
  moveAndBackup "$f"
done
