
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

# bspwm settings
PANEL_FIFO=/tmp/panel-fifo
PANEL_HEIGHT=24
PANEL_FONT_FAMILY="-*-terminus-medium-r-normal-*-12-*-*-*-c-*-*-1"
export PANEL_FIFO PANEL_HEIGHT PANEL_FONT_FAMILY
export PATH=$PATH:$HOME/.scripts
