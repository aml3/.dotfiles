set nocompatible

filetype off
filetype plugin indent on

call plug#begin('~/.vim/plugged')
  " lisp
    Plug 'kovisoft/slimv'
call plug#end()

" slimv
let g:paredit_mode=0

function! NumberToggle()
  if(&relativenumber == 1)
    set nu
    set nornu
  else
    set rnu
    set nonu
  endif
endfun

set rnu
nnoremap <C-n> :call NumberToggle()<cr>
set smartindent
set autochdir
syntax on

set textwidth=80
set tabstop=2
set shiftwidth=2
set expandtab

" Only use the colorscheme if we aren't in a tty
if (match(system("echo $TERM"), "linux") == -1)
	set t_Co=256
	colorscheme 256-grayvim
endif

if has("autocmd")
  autocmd BufWinLeave *.* mkview
  autocmd BufWinEnter *.* silent loadview

  " spellchecking for .md and .tex files
  autocmd BufWinEnter *.md set spell
  autocmd BufWinEnter *.tex set spell

	autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
  autocmd BufRead,BufNewFile *.md set filetype=markdown

  augroup latex
   autocmd! 
   "autocmd BufWritePost *.tex !pdflatex %
   autocmd FileType tex command W :w | !pdflatex %
  augroup END

  augroup corewar
    autocmd!
    autocmd FileType rc command R !pmars %
  augroup END
endif
