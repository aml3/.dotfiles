#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#alias ls='ls --color=auto'
alias ls='ls --color=auto'
LS_COLORS='di=1;4' #:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35:'
export LS_COLORS

PS1='[\u@\h \W]\$ '
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

export INFOPATH=$INFOPATH:/usr/local/texlive/2013/texmf-dist/doc/info
export MANPATH=$MANPATH:/usr/local/texlive/2013/texmf-dist/doc/man
export PATH=$PATH:/usr/local/texlive/2013/bin/x86_64-linux

#if [ -f /etc/bash_completion ]; then
#. /etc/bash_completion
#fi

# export PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting. Added above
export PATH=$PATH:$HOME/.cabal/bin # Cabal
export PATH=$PATH:/usr/local/texlive/2014/bin/x86_64-linux # tlmgr
#alias cool='~/uva/cs4610/cool'
alias lwriter='libreoffice --writer'
alias limpress='libreoffice --impress'
export NODE_PATH="~/node_modules"
export EDITOR=vim
#export TERM='xterm-256color'

alias red='redshift -l 38:-78 -t 4400:2400 -g 0.8 -m vidmode -v'

alias tmux='tmux -2'

alias lockscr='xscreensaver-command -lock'

alias pmars='pmars -fn fixed'

export OCAMLLIB=/usr/lib/ocaml/

#alias mutt_bgrun=~/scripts/mutt_bgrun
#alias RunningX=~/scripts/RunningX
export PATH="$PATH:$HOME/.scripts"
export PATH="$PATH:$HOME/.bin"
export PATH="$PATH:$HOME/packages/neovim/build/bin"
