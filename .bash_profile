#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
source ~/.profile

# OPAM configuration
#. /home/alex/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true
